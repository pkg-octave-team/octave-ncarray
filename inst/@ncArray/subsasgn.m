## Copyright (C) 2012 Alexander Barth <barth.alexander@gmail.com>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn  {} {} subsasgn(@var{A}, @var{idx}, @var{rhs})
## Perform the subscripted assignment operation according to the subscript specified by idx.
##
## A slice of the NetCDF variable can be saved by using A(index1,index2,...) = rhs;
## @subsubheading Inputs
## @var{A} - ncArray value
##
## @var{idx} - idx structure
##
## @var{rhs} - value to assign
## @subsubheading Outputs
## None
## @end deftypefn

function self = subsasgn(self,idx,x)

  self = subsasgn(self.var,idx,x);

endfunction
